# named-discrete-coordinates-plotters
Easily create enum-based [plotters](https://github.com/plotters-rs/plotters) discrete coordinates, for trivial segmentation of coordinate axes. 

Useful in combination with [`plotters::coord::combinators::NestedRange`] and my crate `uniform-nested-coords-plotters`, which contains a similar structure `UniformNestedRange`. 

This crate primarily provides a macro to generate enums which implement [`plotters::coord::ranged1d::Ranged`] and [`plotters::coord::ranged1d::DiscreteRanged`] - or, implement it on an existing enum. This can then be used as any other range. 
